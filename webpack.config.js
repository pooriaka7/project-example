const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const {merge} = require('webpack-merge');


const pagesList = ['index', 'register', 'charts'];
let htmlMultiplesFiles = pagesList.map(function (entryName) {
    return new HtmlWebpackPlugin({
        filename: entryName + ".html",
        template: "src/template/pages/" + entryName + ".html",
        chunks: [entryName],
    });
});

const config = {
    entry: {
        common: path.resolve(__dirname, 'src/js/common/common.js'),
        index: path.resolve(__dirname, 'src/js/pages/index.js'),
        charts: path.resolve(__dirname, 'src/js/pages/charts.js'),
        register: path.resolve(__dirname, 'src/js/pages/register.js')
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "js/[name].js",
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },

    module: {
        rules: [
            {
                test: /.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-transform-runtime']
                    }
                }
            },
            {
                test: /.(sc|sa|c)ss$/,
                use: [miniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(html)$/,
                loader: "html-loader",
                options: {
                    interpolate: true,
                },
            },
            {
                test: /.(eot|ttf|woff|woff2|svg)$/,
                loader: 'file-loader',
                include: path.resolve(__dirname, 'src/webfonts'),
                options: {
                    name: '[name].[ext]',
                    outputPath: 'webfonts',
                    publicPath: "../webfonts"
                }
            },
        ]
    },
    plugins: [
        new miniCssExtractPlugin({
            filename: 'css/[name].css'
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
        })
    ].concat(htmlMultiplesFiles),
};

const devConfig = merge(config, {
    mode: "development",
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        port: 8000,
        hot: true
    }
});

const prodConfig = merge(config, {
    mode: "production",
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/](node_modules)[\\/](jquery|bootstrap)/,
                    name: 'vendor',
                    chunks: 'initial',
                    filename: 'js/vendor/[name].js'
                }
            }
        }
    },
});

module.exports = process.env.NODE_ENV === 'development' ? devConfig : prodConfig;