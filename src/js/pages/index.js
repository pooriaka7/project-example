import '../../sass/pages/index.sass';
import $ from 'jquery';
import '../common/common'

$(document).ready(() => {
    function insertDataTable() {
        let table_body = $('table tbody'), data = JSON.parse(localStorage.getItem('users'));
        if (data === null) {
            table_body.append('<tr><td colspan="5">no data exists (add some users)</td></tr>');
        } else {
            data.map(item=>{
                table_body.append('<tr>' +
                    `<td>${item.firstname}</td>` +
                    `<td>${item.lastname}</td>` +
                    `<td>${item.age}</td>` +
                    `<td>${item.field}</td>` +
                    `<td>${item.entryYear}</td>` +
                    '</tr>')
            })
        }
    }

    insertDataTable();
});