import $ from 'jquery';
import '../../sass/common/common.sass';
import '../common/common'
import Highcharts from 'highcharts';

$(document).ready(() => {
    function getItemsLocalStorage(key) {
        return JSON.parse(localStorage.getItem(key));
    }

    let user_data = getItemsLocalStorage('users'), select_field = $("#field-select");
    let filteredDataOnselect, defaultData, pieChartData = [];

    function createDefault() {
        if (localStorage.getItem('filteredData') === null) {
            localStorage.setItem('filteredData', []);
        }
    }

    createDefault();

    //grouping data based on age
    function groupByAge(list) {
        return list.reduce((r, a) => {
            r[a.age] = r[a.age] || [];
            r[a.age].push(a);
            return r;
        }, {});
    }

    function arrayOfGroupedDataByAge(list) {
        Object.keys(list).forEach(key => {
            pieChartData.push(
                {
                    name: `number of users of ${key} years old`,
                    age: key,
                    //number of users in specific age
                    y: list[key].length
                }
            )
        })
    }

    //load default value of select option on load
    function loadDefaultPieChart() {
        defaultData = user_data.filter(data => {
            if (data.field === select_field.val()) {
                return data;
            }
        })
        arrayOfGroupedDataByAge(groupByAge(defaultData));
    }

    loadDefaultPieChart();

    //pie chart
    function createPieChart() {
        Highcharts.chart('piechart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Number of users that in a specific field with age of them'
            },
            tooltip: {
                pointFormat: ` <b>{point.y}</b>`
            },
            accessibility: {
                point: {
                    valueSuffix: ''
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y}'
                    }
                }
            },
            series: [{
                colorByPoint: true,
                data: pieChartData
            }]
        });
    }

    createPieChart();
    select_field.on('change', function () {
        filteredDataOnselect = user_data.filter(data => {
            if (data.field === $(this).val())
                return data;
        })
        pieChartData = [];
        arrayOfGroupedDataByAge(groupByAge(filteredDataOnselect));
        createPieChart();
    })

    //grouping data based on entry year
    function groupByEntryYear(list) {
        return list.reduce((r, a) => {
            r[a.entryYear] = r[a.entryYear] || [];
            r[a.entryYear].push(a);
            return r;
        }, {});
    }

    let lineBarChartData = [],user=[];

    function arrayOfGroupedDataByEntryYear(list) {
        Object.keys(list).forEach(key => {
            user.push(
                list[key]
            )
        })
    }

    function loadDefaultLineBarChart() {
        arrayOfGroupedDataByEntryYear(groupByEntryYear(user_data));
        user.forEach(numOfEntryYear=>{
            lineBarChartData.push(numOfEntryYear.length);
        })
    }

    loadDefaultLineBarChart();

    //line bar Chart
    function createLineBar() {
        Highcharts.chart('barchart', {

            title: {
                text: 'number of users per year of entry'
            },

            yAxis: {
                title: {
                    text: 'number of users'
                }
            },

            xAxis: {
                allowDecimals: false,
                title: {
                    text: 'entry years'
                }
            },

            legend: false,

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 92
                }
            },

            series: [{
                name: 'number of users registered',
                data: lineBarChartData
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }

    createLineBar();
});

