import $ from 'jquery';
import '../common/common';
import 'bootstrap/js/dist/modal';
import '../../sass/pages/register.sass'
import {
    wordValidation,
    numValidation,
    notValid,
    selectValidation
} from '../functions/functions';

$(document).ready(function () {
    let inputs = $('.inputs'), submit_btn = $('#submit'), modal_result = $("#modal-result");
    const validation = (input, status) => {
        if (input.is('#firstname'))
            !input.val() ? notValid(input, 'Enter your firstname', status) : wordValidation(input, 'your firstname isn\'t valid', status);

        if (input.is('#lastname'))
            !input.val() ? notValid(input, 'Enter your lastname', status) : wordValidation(input, 'your lastname isn\'t valid', status);

        if (input.is('#age'))
            !input.val() ? notValid(input, 'Enter your age', status) : numValidation(input, 'your age isn\'t valid', status);

        if (input.is("#entryYear"))
            !input.val() ? notValid(input, 'Enter your entry year', status) : numValidation(input, 'your entry year isn\'t valid', status);
    };

    inputs.on('change', function () {
        validation($(this));
    });
    //default array value of localstorage
    let data=JSON.parse(localStorage.getItem('users'))===null ? [] : JSON.parse(localStorage.getItem('users'));

    submit_btn.on('click', function () {
        let formStatus = [];
        inputs.each(function () {
            validation($(this), formStatus);
        })
        selectValidation($('#field'), 'Select your field', formStatus);
        if (!formStatus.includes(false)) {
            let firstnameVal = $("#firstname").val(), lastnameVal = $("#lastname").val(), ageVal = $("#age").val(),
                fieldVal = $("#field").val(), entryYearVal = $("#entryYear").val();
            let userData = {
                firstname: firstnameVal,
                lastname: lastnameVal,
                age: parseInt(ageVal),
                field: fieldVal,
                entryYear: parseInt(entryYearVal)
            };
            data.push(userData);
            localStorage.setItem('users',JSON.stringify(data));
            console.log(JSON.parse(localStorage.getItem('users')));
            modal_result.modal('show');
            inputs.val('');
        }
    })
});