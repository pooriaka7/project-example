//regex
const wordRegex = /^[a-zA-Z][a-z A-Z]*$/;
const numRegex = /^[0-9]+$/;




//validations
export const notValid=(input,errMsg,status)=>{
    input.addClass('not-valid');
    input.siblings('label.error').text(errMsg);
    if(status){
        status.push(false);
    }
}
const valid=(input)=>{
    input.removeClass('not-valid');
    input.siblings('label.error').text(null);
}

export const wordValidation = (input, errMsg,status) => {
    if (!wordRegex.test(input.val().trim())) {
        notValid(input,errMsg,status);
    } else {
       valid(input);
    }
}

export const numValidation = (input, errMsg,status) => {
    if (!numRegex.test(input.val().trim())) {
        notValid(input,errMsg,status);
    }else{
        valid(input);
    }
}

export const selectValidation=(input, errMsg,status)=>{
    if(!input.val().trim()){
        notValid(input,errMsg,status);
    }else{
        valid(input);
    }
}
//end validation


