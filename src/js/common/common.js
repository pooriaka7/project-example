import $ from 'jquery';
import '../../sass/common/common.sass';

$(document).ready(()=>{
    const common={
        handleToggleMenu(){
            let menuToggle=$('#menu-toggler'),menu=$('#nav-menu');
            menuToggle.on('click',function (){
                if (window.outerWidth < 768) {
                    if (!$(this).hasClass('active')) {
                        $(this).addClass('active');
                        menu.slideDown(200);
                    } else {
                        $(this).removeClass('active');
                        menu.slideUp(200);
                    }
                }
            });
        },
        init(){
            this.handleToggleMenu()
        }
    }
    common.init();
})